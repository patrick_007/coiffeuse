import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { Utilisateur } from 'Models/Utilisateur';
import { AuthentificationService } from 'src/Services/auth/authentification.service';
import { CommunFunctionService } from 'src/Services/CommunFunctions/communFunction.service';
import { UtilisateurService } from 'src/Services/Utilisateur/utilisateur.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {

  clientForm!: FormGroup;

  isEmailGood!: boolean;
  isPhoneGood!: boolean;
  isPaswordGood!: boolean;
  isValidationGood!: boolean;
  isButtonConnectClicked!: boolean;

  errorMessage!: string;
 


  constructor(public currentServ: CommunFunctionService, public userServ: UtilisateurService, private formBuilder: FormBuilder, public authserv : AuthentificationService, private router: Router) {
    this.initClassVar();
  }

  ngOnInit(): void {
  }


  /**
   * cette fonction permet d'inscrire ou d'ajoutter une CLiente dans la 
   * base de donnée 
   */
  async ajoutterUtilisateur(data: any) {
    this.isButtonConnectClicked = true;
    const newClient : Utilisateur = new Utilisateur(data.nom, data.email, data.telephone, 'client', undefined, undefined, false, data.password, );
    await this.isValidData(newClient).then(resultSet => {
      if(resultSet === true) {
        this.userServ.AjoutterUnClient(newClient).subscribe(async (data) => {
          if(data) {
              await this.authserv.SignOut();
              await this.authserv.SignIn(newClient.email, newClient.password);
              await this.currentServ.redirectTo('acceuil');
          }
        }, error => {
          if(error.error.message.ToString().includes("phone")) {
            this.isPhoneGood = false;
            this.isPaswordGood = true;
            this.isEmailGood = true;
            this.isButtonConnectClicked = true;
            this.errorMessage = 'Verifiez le format du numero de téléphone !!!';
          }
        });
      }
    }); 
  }

  /**
   * Cette fonction permet de Tester si les données entrés sont valides 
   * @param client : Utilisateur
   * @returns 
   */
  isValidData(client: Utilisateur) {
    return new Promise(resolved => {
      if(client.email === '' || client.email.length === 0) {
        this.isEmailGood = false;
        this.errorMessage = 'veuillez entrer une adresse mail ';
        resolved(false);
       } else if(client.telephone === '+237' || client.telephone === '' || client.telephone.length === 0) {
        this.isPhoneGood = false;
        this.isPaswordGood = true;
        this.isEmailGood = true;
        this.errorMessage = 'Verifiez votre numero de téléphone'
        resolved(false);
      }else if(client.password === '' || client.password.length === 0) {
         this.isPaswordGood = false;
         this.isPhoneGood = true;
          this.isEmailGood = true;
         this.errorMessage = 'Veuillez entrer un mot de passe ';
         resolved(false);
       }
        else {
        resolved(true);
       }
    });
  }


  /**
   * initialisation des variables de la classe 
   */
  initClassVar() {
      this.clientForm = this.formBuilder.group({ 
        email: ['', [Validators.required, Validators.email]],
        password:  ['', [Validators.required, Validators.maxLength(40)]],
        telephone: ['', [Validators.required]],
        accepted: [true, [Validators.required, Validators.requiredTrue]],
        nom:[''],
        role: ['client']
      });
      this.isButtonConnectClicked = false;
      this.isEmailGood = true;
      this.isPhoneGood = true;
      this.isPaswordGood = true;
      this.isValidationGood = true;
    }
}
