import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthentificationService } from 'src/Services/auth/authentification.service';

@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.scss']
})
export class AcceuilComponent implements OnInit {

  active_tab_one : string | undefined;
  active_tab_two!: string;
  isSearchActiveTab!: boolean;
  listClientes!: [number,number, number];
  IsNotDeployed!: boolean;
  isCardOver!: boolean;

  closeResult: string = '';


  constructor(private modalService: NgbModal, private router: Router, public authServ: AuthentificationService) {
    this.activeTab(1);
    this.initClassVar();
  }

  ngOnInit(): void {
  }



  

 



   /**
   * Permet de naviguer entre les Tabulation de reervation et 
   * de decouverte des coiffeuses 
   * @param indexTab 
   */
    activeTab(indexTab: number) {
      if(indexTab && indexTab === 1) {
        this.active_tab_one = "active";
        this.active_tab_two = "";
        this.isSearchActiveTab = true;
      } else {
        this.active_tab_one = "";
        this.active_tab_two = "active";
        this.isSearchActiveTab = false;
      }
    }
  
    /**
     * Cette fonction permet d'initialiser les variable de la classe 
     * courante 
     */
    initClassVar() {
      this.listClientes = [1,1,1];
      this.IsNotDeployed = true;
      this.isCardOver = false;
    }
  
    changeButtonMenu(indexButton : number) {
      if(indexButton === 1) {
        this.IsNotDeployed = false;
      }
      else {
        this.IsNotDeployed = true;
      }
    }
  
    activateButton() {
      this.isCardOver = true;
    }
  

}
