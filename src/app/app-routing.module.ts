import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AcceuilComponent } from './pages/acceuil/acceuil.component';
import { CreationConnexionComponent } from './pages/creation-connexion/creation-connexion.component';
import { InscriptionComponent } from './pages/inscription/inscription.component';

const routes: Routes = [
  { 
      path: 'acceuil', 
      component: AcceuilComponent 
  },
  {
    path: 'acceuil/1',
    component: AcceuilComponent
  },
  { 
    path: 'connexion-compte', 
    component: CreationConnexionComponent 
  },
  { 
    path: 'inscription', 
    component: InscriptionComponent 
  },
  {
    path:'', 
    redirectTo: 'acceuil',
    pathMatch: 'full'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
