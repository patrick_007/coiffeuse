import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthentificationService } from 'src/Services/auth/authentification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'wbgmarket3';
  active_tab_one : string | undefined;
  active_tab_two!: string;
  isSearchActiveTab!: boolean;
  isCardOver!: boolean;
 
  constructor(private router: Router, private modalService: NgbModal, public authServ: AuthentificationService) {
    this.initClassVar();
  }

  /***
   * initialisation des variables de la classes courante 
   */
  initClassVar() {
    this.isCardOver = false;
  }



 

}
