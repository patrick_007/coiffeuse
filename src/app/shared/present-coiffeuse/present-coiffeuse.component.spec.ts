import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentCoiffeuseComponent } from './present-coiffeuse.component';

describe('PresentCoiffeuseComponent', () => {
  let component: PresentCoiffeuseComponent;
  let fixture: ComponentFixture<PresentCoiffeuseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PresentCoiffeuseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentCoiffeuseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
