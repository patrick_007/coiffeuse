import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthentificationService } from 'src/Services/auth/authentification.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  IsNotDeployed!: boolean;
  closeResult: string = '';
  isConnected = false;


  constructor(private router: Router, public authServ: AuthentificationService, private modalService: NgbModal) {
    this.initClassVar();
   }

  ngOnInit(): void {
  }

   /***
   * initialisation des variables de la classes courante 
   */
    initClassVar() {
      this.IsNotDeployed = true;
    }

     /**
   * changement de tabulation sur le box de recherche de la page d'acceuil
   * @param indexButton 
   */
  changeButtonMenu(indexButton : number) {
    if(indexButton === 1) {
      this.IsNotDeployed = false;
    }
    else {
      this.IsNotDeployed = true;
    }
  }

      /**
     * cette fonction permet d'ouvri un modal de gestion de compte 
     * @param content : Modal la classe modal ;
     */
       open(content:any) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
      }

       /**
   * Cette fonction permet de se deconnecter de l'application , 
   * cet a dire deconnecter son compte Utilisateur du navigateur 
   * il fait simplement appel au service d'authentification pour effectuer cette tache 
   */
    async seDeconnecter() {
      this.isConnected = false;
      this.modalService.dismissAll();
      await this.authServ.SignOut();
      this.reloadCurrentRoute();
    }

    /**
     * 
     */
    reloadCurrentRoute() {
      window.location.reload();
    }

      /**
   * Cette fonction permet de naviguer vers une autre page 
   * a partir de son URL , on vas l'externaliser dans un service 
   * lors du refractoring
   * @param cheminUrl :: string Url de la page a atteindre 
   */
  redirectTo(cheminUrl: string) {
    this.router.navigateByUrl(cheminUrl);
  }

  

}
