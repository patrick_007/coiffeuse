import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AuthentificationService } from 'src/Services/auth/authentification.service';
import { UtilisateurService } from 'src/Services/Utilisateur/utilisateur.service';
import { CommunFunctionService } from 'src/Services/CommunFunctions/communFunction.service';
import { AcceuilComponent } from './pages/acceuil/acceuil.component';
import { CreationConnexionComponent } from './pages/creation-connexion/creation-connexion.component';
import { InscriptionComponent } from './pages/inscription/inscription.component';
import { PresentCoiffeuseComponent } from './shared/present-coiffeuse/present-coiffeuse.component';
import { GlobalParameter } from 'CONFIG_APP/init_Application';
import { FooterComponent } from './shared/footer/footer.component';
import { HeaderComponent } from './shared/header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { ToastrCustumService } from 'src/Services/Toastr/Toastr.service';

@NgModule({
  declarations: [
    AppComponent,
    AcceuilComponent,
    CreationConnexionComponent,
    InscriptionComponent,
    PresentCoiffeuseComponent,
    FooterComponent,
    HeaderComponent
  ],
  imports: [
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    AngularFirestoreModule, 
    AngularFireAuthModule, 
    AngularFireStorageModule 
  ],
  providers: [ToastrCustumService, AuthentificationService, UtilisateurService, CommunFunctionService, GlobalParameter],
  bootstrap: [AppComponent],
  exports: [FormsModule]
})
export class AppModule {
}
