import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth, AngularFireAuthModule } from '@angular/fire/auth'
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Utilisateur } from 'Models/Utilisateur';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  public userData: any; // Save logged in user data
  isUserConnected!: boolean;

  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,  
    public ngZone: NgZone // NgZone service to remove outside scope warning
  ) { 

    /* Saving user data in localstorage when 
    logged in and setting up null when logged out */
    this.isUserConnected = false;
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        this.isUserConnected = true;
        localStorage.setItem('user', JSON.stringify(this.userData));
      } else {
        // localStorage.setItem('user',);
        // JSON.parse(localStorage.getItem('user'));
      }
    });
  }

  // Sign in with email/password
  async SignIn(email: string , password: string ) {
    return new Promise(async resolved => {
      await this.afAuth.signInWithEmailAndPassword(email, password)
      .then(result => {
        this.ngZone.run(() => {
          this.router.navigate(['acceuil']);
        });
        this.SetUserData(result.user);  
        this.isUserConnected = true;
        resolved(true);
      }).catch(error => {
        this.setErrorMessage(error).then(resulTraitement => {
          resolved(resulTraitement);
        });
    });
    })
  }

  setErrorMessage(error: any) {
    return new Promise(resolved => {
      if (error && error.code === 'auth/user-not-found') {
        const object = {
          field : "email",
          message: "il n'existe pas d'utilisateur avec cet adresse"
        }
        resolved(object);
      }
      if (error && error.code === 'auth/wrong-password') {
        const object = {
          field : "password",
          message: "le Mot de passe est incorect !!! "
        }
        resolved(object);
      }
    });
   
  }

  // Sign out 
  async SignOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigateByUrl('acceuil');
    });
  }

  // Sign up with email/password
  // SignUp(email, password) {
  //   return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
  //     .then((result) => {
  //       /* Call the SendVerificaitonMail() function when new user sign 
  //       up and returns promise */
  //       this.SendVerificationMail();
  //       this.SetUserData(result.user);
  //     }).catch((error) => {
  //       window.alert(error.message)
  //     })
  // }

  // Reset Forggot password
  // ForgotPassword(passwordResetEmail) {
  //   return this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail)
  //   .then(() => {
  //     window.alert('Password reset email sent, check your inbox.');
  //   }).catch((error) => {
  //     window.alert(error)
  //   })
  // }

   // Returns true when user is looged in and email is verified
  //  get isLoggedIn(): boolean {
  //   const user = JSON.parse(localStorage.getItem('user'));
  //   return (user !== null && user.emailVerified !== false) ? true : false;
  // }

  // Sign in with Google
  // GoogleAuth() {
  //   return this.AuthLogin(new auth.GoogleAuthProvider());
  // }

  // Auth logic to run auth providers
  // AuthLogin(provider) {
  //   return this.afAuth.auth.signInWithPopup(provider)
  //   .then((result) => {
  //      this.ngZone.run(() => {
  //         this.router.navigate(['dashboard']);
  //       })
  //     this.SetUserData(result.user);
  //   }).catch((error) => {
  //     window.alert(error)
  //   })
  // }

  // Send email verfificaiton when new user sign up
  // SendVerificationMail() {
  //   return this.afAuth.auth.currentUser.sendEmailVerification()
  //   .then(() => {
  //     this.router.navigate(['verify-email-address']);
  //   })
  // }

  /* Setting up user data when sign in with username/password, 
  sign up with username/password and sign in with social auth  
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  SetUserData(user: any) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: Utilisateur = new Utilisateur(user.displayName, user.email, user.phoneNumber, 'client', user.uid,
    user.photoURL, user.emailVerified, user.password);
    this.userData = userData;
    localStorage.setItem('user', JSON.stringify(userData));
    return userRef.set(userData, {
       merge: true
    });
  }

   
}
