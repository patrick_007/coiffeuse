import { Injectable, NgZone } from '@angular/core';
import { Utilisateur } from 'Models/Utilisateur';
import { HttpClient } from '@angular/common/http';
import { GlobalParameter } from 'CONFIG_APP/init_Application';


@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  headers = { 'content-type': 'application/json'}
  baseUrlUser = '';

  constructor( private param: GlobalParameter, private http: HttpClient) { 
     this.baseUrlUser = this.param.ServeurApi + "Users";
  }

   /**
    * ce service permet d'ajoutter un Utilisateur 
    * @param objetClient :: Utilisateur 
    * @returns <Observable>Utilisateur</Observable>
    */
   AjoutterUnClient(objetClient: Utilisateur) {
    const body=JSON.stringify(objetClient);
    debugger;
    return this.http.post(this.baseUrlUser, body,{'headers':this.headers});
   }
}
