import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class ToastrCustumService {
  

  constructor(private toastr: ToastrService) { }

  montrerleToastDeSucces(message: string) {
    this.toastr.success(message, 'bgm_ information !',
    {timeOut: 2000});
  }
  

}
