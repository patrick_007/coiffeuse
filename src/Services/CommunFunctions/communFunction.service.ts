import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class CommunFunctionService {
  

  constructor(private router: Router) { }

  /**
   * cette fonction permet de rediriger la navigation vers un Url precise 
   * @param urlChemin :: String chemin de lurl qu'on veux atteindre
   */
  redirectTo(urlChemin: string) {
    this.router.navigateByUrl(urlChemin);
  }
  

}
