export class Utilisateur {
    uid?: string;
    nom: string;
    email: string; 
    password: string; 
    role: string;
    photoURL?: string;
    emailVerified?: boolean;
    telephone: string;

    /**
     * Ceci est le constructeur de l'object Utilisateur 
     * @param nom :: nom de l'utilisateur
     * @param email :: email de l'utilisateur
     * @param telephone :: telephone de l'utilisaeur 
     * @param role :: role es ce un client ou une coiffeuse
     * @param uid :: string Id de l'utilisaeur 
     * @param photoURL :: Url de l'image utilisateur 
     * @param emailVerified :: email de l'utilisateur
     * @param password :: Mot de passe
     */
    constructor(nom: string, email: string, telephone: string, role: string, uid?: string, photoURL?: string, emailVerified?:boolean, password?: string) {
        this.nom = nom;
        this.email = email,
        this.password = password ? password : "hidden";
        this.telephone = "+237" + telephone;
        this.role = role;
        this.uid = uid ? uid : undefined;
        this.photoURL = photoURL ? photoURL : undefined;
        this.emailVerified = emailVerified ? emailVerified : false;
    }

}